import { CoreSeries } from '@chartwerk/core';
import { ScatterData, PointType, LineType, ScatterDataParams } from '../types';

import * as _ from 'lodash';

const DEFAULT_POINT_SIZE = 4;

const SCATTER_DATA_DEFAULTS: ScatterDataParams = {
  pointType: PointType.CIRCLE,
  lineType: LineType.NONE,
  pointSize: DEFAULT_POINT_SIZE,
  colorFormatter: undefined,
  clickCallback: undefined,
};

export class ScatterSeries extends CoreSeries<ScatterData> {

  constructor(series: ScatterData[]) {
    super(series, _.cloneDeep(SCATTER_DATA_DEFAULTS));
  }

  // move to parent
  public getSerieByTarget(target: string): ScatterData | undefined {
    return _.find(this.visibleSeries, serie => serie.target === target);
  }
}
