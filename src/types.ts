import { Serie, Options } from '@chartwerk/core';


export type ScatterDataParams = {
  pointType: PointType;
  lineType: LineType;
  pointSize: number;
  colorFormatter?: ColorFormatter;
  clickCallback?: ( metricData: { target: Target, class: string, alias: string }, pointData?: DelaunayDataRow) => void;
}
type ScatterOptionsParams = {
  // TODO: this options is not used anywhere, let's remove it
  voronoiRadius: number;
}
export type ScatterData = Serie & Partial<ScatterDataParams>;
export type ScatterOptions = Options & Partial<ScatterOptionsParams>;

export enum PointType {
  NONE = 'none',
  CIRCLE = 'circle',
  RECTANGLE = 'rectangle'
}

export enum LineType {
  NONE = 'none',
  SOLID = 'solid',
  DASHED = 'dashed'
}

export type ColorFormatter = (datapointsRow: any[], pointIdx) => string;

export type MouseMoveEvent = {
  bbox: {
    clientX: number,
    clientY: number,
    x: number,
    y: number,
    chartWidth: number,
    chartHeight: number,
  },
  data: {
    xval: number,
    yval: number,
    highlighted?: HighlightedData,
  }
}

export type HighlightedData = {
  xValue: number, yValue: number, customValue: number,
  pointIdx: number, totalPointIdx: number,
  serieInfo: { target: string, alias?: string, class?: string, idx?: number }
}


type Value = number;
type PointIdx = number;
type Target = string;
// type row: [ 0:x, 1:y, 2:(custom value | null), 3:pointIdx, 4:serie.target ]
export type DelaunayDataRow = [Value, Value, Value | null, PointIdx, Target];
